# lpcats
Low Profile, Curved And Thin, Split keyboard

## What is it?
A keyboard based on my other project [CKC](https://codeberg.org/ragnar/ckc). This repo will maybe, at some point, contain a build guide.. or at the very least the ckc-config.. which will probably also be an example config in CKC.

## Storytime
What i originally wanted was just to have a split keyboard with configurable columns (shape, number of keys, rotations, etc) and thumbcluster, and then for the rest (walls and base) be generated automagically. As i started writing it tho, it turned more and more into a keyboard-generator than just some extra config-options for a predefined keyboard - so, what i first intended to just be lpcats, became CKC. So this repo is likely to be pretty empty until i get CKC to usable state.

### BOM
Since i am a AE-shopoholic i have already bought most of the components.

| Part       | Quantity | Unit Price | Total  | Shipping | Store      | Notes |
| :--------- | :------- | :--------- | :----- | :------- | :--------- | :---- |
| Switches   | 4 x12    | \$4.00     | \$16.00 | \$9.00  | [Keychron](https://www.keychron.com/products/low-profile-gateron-mechanical-switch-set) | Gateron LP blues ||
| Caps       | 1 x117   | \$14.21    | \$14.21 | \$3.14  | Aliexpress | XVX Horizon low profile |
| Diodes     | 1 x100   | €2.15      | €2.15  | €0.25    | ebay       | 1N4148 |
| BLE boards | 2        | \$4.54      | \$9.08  | \$3.62 | Aliexpress | [MS88SF21](https://www.minew.com/products/nrf52840-module-ms88sf2.html), SoC: [nRF52840](https://www.nordicsemi.com/Products/Low-power-short-range-wireless/nRF52840) ||
| USB-C breakouts | 2   | \$0.9       | \$1.80  | \$1.17    | Aliexpress | ||
| Battery management | 2 | \$1.95     | \$3.90  | \$3.83    | Aliexpress | MCP73871 load sharing board ||
| Thermistors | 1 x10   | €2.99      | €2.99  | €0.99    | ebay       | NTC 10kΩ 5%, MF52 for battery overheating protection |
| Lipo batteries, 2Ah | 2 | \$4.20    | \$8.40  | $0       | Aliexpress | ||

So just under \$60 (excluding consumables and shipping), of course the two most costly items - switches and caps - might run you a lot more a quite a bit less depending on your taste. The shipping, \$22 in total, is to sweden and it's worth noting that you can (or well, _could_, given chip shortages, wars, and so on) get most of the other AE parts cheaper than i did if you can order stuff from china without getting any extra taxes/fees added to them - i pretty much only bought stuff from sellers that could ship from the EU. So all-in-all it's pretty price competitive with even lower end prebuilds, and if you compare it to most sculpted puppers you can throw in an AE 3D-printer and still come out way below average.. given, of course, that you do not value your own time =)

## License
Unlicense/public domain